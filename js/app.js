var twitter = {
    limit: 140,
    textAreaId: document.getElementById("js-message"),
    totalSymbols: document.getElementById("js-message-left-total"),
    remainingSymbols: document.getElementById("js-message-left-symbols"),
    
    init: function () {
        this.textAreaId.maxLength = this.limit;
        this.setRemainingSymbols();
        this.events();
    },
    setTotalSymbols: function () {
        this.totalSymbols.innerText = this.textAreaId.value.length;
    },
    setRemainingSymbols: function () {
        this.remainingSymbols.innerText = this.limit - this.getTotalSymbols();
    },
    getTotalSymbols: function () {
        return this.totalSymbols.innerText;
    },
    events: function () {
        var self = this;
        self.textAreaId.addEventListener("keyup", function () {
            self.setTotalSymbols();
            self.setRemainingSymbols();
        });
    }
}

twitter.init();